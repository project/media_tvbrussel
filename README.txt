Media: TV BRUSSEL

This adds support for the TV Brussel video sharing service, available at
http://www.tvbrussel.be.

To use this module, you'll first need to install Embedded Video Field, which is
packaged with Embedded Media Field (from http://drupal.org/project/emfield).

Set up a content type to use a Third Party Video field as you normally would
with emfield. Also ensure that you have enabled the new TV Brussel provider from
the Admin screen at /admin/content/emfield.

Only Embed Code (and not the URL) from the embedded third party content will be
parsed and displayed, alongside with the thumbnail.
