<?php

/**
 * @file
 * This is an tvbrussel provider include file for Embedded Media Video.
 * Use this as a base for creating new provider files.
 */

/**
 * This is the main URL for your provider.
 */
define('EMVIDEO_TVBRUSSEL_MAIN_URL', 'http://www.tvbrussel.be/');

/**
 * This defines the version of the content data array that we serialize
 * in emvideo_tvbrussel_data(). If we change the expected keys of that array,
 * we must increment this value, which will allow older content to be updated
 * to the new version automatically.
 */
define('EMVIDEO_TVBRUSSEL_DATA_VERSION', 1.0);

/**
 * Implements EMMODULE_PROVIDER_info().
 *
 * This returns information relevant to a specific 3rd party video provider.
 *
 * @return
 *   A keyed array of strings requested by various admin and other forms.
 *    'provider' => The machine name of the provider. This must be the same as
 *      the base name of this filename, before the .inc extension.
 *    'name' => The translated name of the provider.
 *    'url' => The url to the main page for the provider.
 *    'settings_description' => A description of the provider that will be
 *      posted in the admin settings form.
 *    'supported_features' => An array of rows describing the state of certain
 *      supported features by the provider. These will be rendered in a table,
 *      with the columns being 'Feature', 'Supported', 'Notes'. In general,
 *      the 'Feature' column will give the name of the feature, 'Supported'
 *      will be Yes or No, and 'Notes' will give an optional description or
 *      caveats to the feature.
 */
function emvideo_tvbrussel_info() {
  $features = array(
    array(t('Full screen mode'), t('Yes'), t('You may customize the player to enable or disable full screen playback. Full screen mode is enabled by default.')),
    array(t('Thumbnails'), t('Yes'), t('')),
  );

  return array(
    'provider' => 'tvbrussel',
    'name' => t('TVBrussel'),
    'url' => EMVIDEO_TVBRUSSEL_MAIN_URL,
    'settings_description' => t('These settings specifically affect videos displayed from !tvbrussel.', array('!tvbrussel' => l(t('TVBrussel.be'), EMVIDEO_TVBRUSSEL_MAIN_URL))),
    'supported_features' => $features,
  );
}

/**
 * Implements EMMODULE_PROVIDER_extract().
 *
 * This is called to extract the video code from a pasted URL or embed code.
 *
 * We'll be passed a URL or the embed code from a video when an editor pastes
 * that in the field's textfield. We'll need to either pass back an array of
 * regex expressions to match, or do the matching ourselves and return the
 * resulting video code.
 *
 * @param $parse
 *   An optional string with the pasted URL or embed code.
 *
 * @return
 *   Either an array of regex expressions to be tested, or a string with the
 *   video code to be used. If the hook tests the code itself, it should
 *   return either the string of the video code (if matched), or an empty
 *   array. Otherwise, the calling function will handle testing the embed code
 *   against each regex string in the returned array.
 */
function emvideo_tvbrussel_extract($parse = '') {
  return array(
   '@upload\.tvbrussel\.be%2F([^"\*]+).flv@i',
  );
}

/**
 * Implements EMMODULE_PROVIDER_embedded_link().
 *
 * Returns a link to view the video at the provider's site.
 *
 * @param $video_code
 *   The string containing the video to watch.
 *
 * @return
 *   A string containing the URL to view the video at the original provider's
 *   site.
 */
function emvideo_tvbrussel_embedded_link($video_code) {
  return 'http://www.tvbrussel.be/';
}

/**
 * Implements EMMODULE_PROVIDER_thumbnail().
 *
 * Returns the external url for a thumbnail of a specific video
 * TODO: make the args: ($embed, $field, $item), with $field/$item provided if we need it, but otherwise simplifying things
 *
 * @param $field
 *   The field of the requesting node.
 * @param $item
 *   The actual content of the field from the requesting node.
 *
 * @return
 *   A URL pointing to the thumbnail.
 */
function emvideo_tvbrussel_thumbnail($field, $item, $formatter, $node, $width, $height, $options = array()) {
  return media_tvbrussel_get_thumb($item);
}

/**
 * Helper function to get the thumbnail.
 */
function media_tvbrussel_get_thumb($item) {
  $type = $item['embed'];
  preg_match( '@images%2F([^"\.]+).png@i', $type, $match);

  $thumbvideo =  "http://www.tvbrussel.be/sites/default/files/";
  $thumbvideo .= urldecode($match[0]);

  return $thumbvideo;
}

/**
 * The embedded flash displaying the megavideo video.
 */
function theme_emvideo_tvbrussel_flash($item, $width, $height, $autoplay) {
  $thumb = media_tvbrussel_get_thumb($item);

  $fullscreen = variable_get('emvideo_tvbrussel_full_screen', 1) ? 'true' : 'false';

  $embed_url = $item['value'];
  $output = '';

  if ($item['embed']) {
    $output .= '<embed width="'. $width .'" height="'. $height .'" allowscriptacces="always" allowfullscreen="'. $fullscreen .'" src="http://www.tvbrussel.be/sites/all/themes/tvbrussel/mp/player.swf" flashvars="&backcolor=0x000000&amp;captions.file=http://upload.tvbrussel.be%2FOT%2F' . $embed_url .'_NL.srt&amp;captions.height=368&amp;captions.pluginmode=FLASH&amp;captions.state=false&amp;captions.visible=true&amp;captions.width=630&amp;captions.x=0&amp;captions.y=0&amp;dock=false&amp;file=http%3A%2F%2Fupload.tvbrussel.be%2F' . $embed_url . '.flv&amp;frontcolor=0x646464&amp;image=' . $thumbvideo .'&amp;lightcolor=0x000000&amp;plugins=viral-h%2Ccaptions-h&amp;image=' . $thumb . '&amp;screencolor=0x000000&amp;skin=http%3A%2F%2Fwww.tvbrussel.be%2Fsites%2Fall%2Fthemes%2Ftvbrussel%2Fmp%2Fmodieus.zip&amp;viral.onpause=false&amp;viral.pluginmode=FLASH" />';
  }
  return $output;
}

/**
 * Implements EMMODULE_PROVIDER_video().
 *
 *  This actually displays the full/normal-sized video we want, usually on the
 *  default page view.
 *
 * @param $embed
 *   The video code for the video to embed.
 * @param $width
 *   The width to display the video.
 * @param $height
 *   The height to display the video.
 * @param $field
 *   The field info from the requesting node.
 * @param $item
 *   The actual content from the field.
 *
 * @return
 *   The HTML of the embedded video.
 */
function emvideo_tvbrussel_video($embed, $width, $height, $field, $item, $node, $autoplay) {
  return theme('emvideo_tvbrussel_flash', $item, $width, $height, $autoplay);
}

/**
 * Implements EMMODULE_PROVIDER_preview().
 *
 *  This actually displays the preview-sized video we want, commonly for the
 *  teaser.
 *
 * @param $embed
 *   The video code for the video to embed.
 * @param $width
 *   The width to display the video.
 * @param $height
 *   The height to display the video.
 * @param $field
 *   The field info from the requesting node.
 * @param $item
 *   The actual content from the field.
 *
 * @return
 *   The HTML of the embedded video.
 */
function emvideo_tvbrussel_preview($embed, $width, $height, $field, $item, $node, $autoplay) {
  return theme('emvideo_tvbrussel_flash', $item, $width, $height, $autoplay);
}

/**
 * Implements EMMODULE_PROVIDER_emfield_subtheme().
 *
 * This returns any theme functions defined by this provider.
 */
function emvideo_tvbrussel_emfield_subtheme() {
  $themes = array(
    'emvideo_tvbrussel_flash'  => array(
      'arguments' => array('item' => NULL, 'width' => NULL, 'height' => NULL, 'autoplay' => NULL),
      'file' => 'providers/tvbrussel.inc',
      'path' => drupal_get_path('module', 'media_tvbrussel'),
    ),
  );
  return $themes;
}
